// Recherche et retourne le nombre le plus grand
function maximum2Nombres(nombre1, nombre2) {

};

// Recherche et retourne le nombre le plus grand
function maximum3Nombres(nombre1, nombre2, nombre3) {

};

// Indique si le nombre est positif, négatif ou nul (== 0)
function signe(nombre) {

};

// Selon le nombre de côtés de même longueur,
// indique si un triangle est équilatéral, isocèle ou quelconque.
function typeTriangle(cote1, cote2, cote3) {

};

// Indique le nombre de valeurs identiques
function valeursIdentiques(nombre1, nombre2, nombre3) {

};

// Selon le numéro choisi, indique le jour de la semaine correspondant
// en toutes lettres.
function jourDeLaSemaine(numeroJour) {

};

// Selon le mois renseigné, indiquer le nombre de jours qu'il y a dans ce mois.
function nombreJours(mois) {

};

// Retourne vrai si le premier nombre est dans l'intervalle des deux suivants.
function intervalle(nombre1, nombre2, nombre3) {

};

// Un magasin de photocopies facture 0,10 € les dix premières photocopies,
// 0,09 € les vingt suivantes et 0,08 € au-delà.
// Retourne le prix total en fonction du nombre de photocopies.
function prixPhotocopies(nombre) {

};

// Dans une rue où se pratique le stationnement alternatif,
// du 1 au 15 du mois on se gare du côté des maisons ayant un numéro impair,
// et le reste du mois on se gare de l’autre côté.
// La fonction retourne vrai si l'on s'est garé du bon côté de la rue.
function stationnementAlternatif(jour, numeroMaison) {

};
